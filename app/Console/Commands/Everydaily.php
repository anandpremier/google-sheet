<?php

namespace App\Console\Commands;

use Google\Client;
use Revolution\Google\Sheets\Sheets;
use App\Http\Controllers\Controller as BaseController;
use App\Models\Home;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon as SupportCarbon;
use Illuminate\Console\Command;

class Everydaily extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:sheetUpdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fatch google sheet recode';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /* google credentials */
        $publicPath = public_path('credentials.json');
        $client = new \Google_Client();
        $client->setApplicationName('My PHP App');
        $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
        $client->setAccessType('offline');

        $client->setAuthConfig($publicPath);

        $sheetsData = new \Google_Service_Sheets($client);

        $data = [];
        $currentRow = 2;
        $sheets2 = array();
        $spreadsheetId = '1owsfsHaJb_1m1WR6FwQ8OMUyJ-MtJ9ROes73WK-4JC8';

        $range = 'A825:L';
        $sheets = new Sheets();
        $sheets->setService($sheetsData);
        $values = $sheets->spreadsheet($spreadsheetId)->sheet('Schedule')->range($range)->all();
        //$rows = $sheets->spreadsheets_values->get($spreadsheetId,$range, ['majorDimension' => 'ROWS']);

        Home::truncate();


        if (isset($values)) {
            foreach ($values as $row) {

                if (!empty($row['0'])) {
                    $mailDate = !empty(date('Y-m-d', strtotime($row['0']))) ? date('Y-m-d', strtotime($row['0'])) : null;
                } else {
                    $mailDate = NULL;
                }
                if (!empty($row['1'])) {
                    $type = !empty($row['1']) ? $row['1'] : null;
                } else {
                    $type = NULL;
                }
                if (!empty($row['2'])) {
                    $companyName = !empty($row['2']) ? $row['2'] : null;
                } else {
                    $companyName = NULL;
                }
                if (!empty($row['3'])) {
                    $masterProduct =  !empty($row['3']) ? $row['3'] : null;
                } else {
                    $masterProduct = NULL;
                }
                if (!empty($row['4'])) {
                    $keycode =  !empty($row['4']) ? $row['4'] : null;
                } else {
                    $keycode = NULL;
                }
                if (!empty($row['5'])) {
                    $quantity =  !empty(str_replace(',', '', $row['5'])) ? str_replace(',', '', $row['5']) : null;
                } else {
                    $quantity = NULL;
                }
                if (!empty($row['6'])) {
                    $actualQuantity =  !empty(str_replace(',', '', $row['6'])) ? str_replace(',', '', $row['6']) : null;
                } else {
                    $actualQuantity = NULL;
                }
                if (!empty($row['7'])) {
                    $offer =  !empty($row['7']) ? $row['7'] : null;
                } else {
                    $offer = NULL;
                }
                if (!empty($row['8'])) {
                    $number =   !empty($row['8']) ? $row['8'] : null;
                } else {
                    $number = NULL;
                }

                /* google sheet recode store */
                $saveRecode = Home::create([
                    'mailDate' => $mailDate,
                    'type' => $type,
                    'companyName' =>  $companyName,
                    'masterProduct' => $masterProduct,
                    'keycode' => $keycode,
                    'quantity' => $quantity,
                    'actualQuantity' => $actualQuantity,

                    'offer' => $offer,
                    'number' =>  $number,
                    'created_at' => Carbon::now(),

                ]);
            }
            if ($saveRecode) {
                return "Record saved successfully";
            } else {
                return "Something went wrong";
            }
        }
    }
}
