<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    protected $table = 'google_spreadsheet_schedule';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['mailDate','type','companyName','masterProduct','keycode','quantity'
    ,'actualQuantity','offer','number','created_at'];
    
}
